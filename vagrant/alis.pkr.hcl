locals {
  ssh_password = "vagrant"
  output_box = "./output_box"
  arch_version = "{{isotime \"2006.01\"}}"
}

source "virtualbox-iso" "arch-linux" {
  guest_os_type = "ArchLinux_64"
  guest_additions_mode = "disable"
  headless = true
  http_directory = "."
  vboxmanage = [
    ["modifyvm", "{{.Name}}", "--memory", "4048"],
    ["modifyvm", "{{.Name}}", "--vram", "128"],
    ["modifyvm", "{{.Name}}", "--cpus", "4"],
    ["modifyvm", "{{.Name}}", "--firmware", "efi"]
  ]
  disk_size = 16384
  hard_drive_interface = "sata"
  iso_url = "https://mirror.rackspace.com/archlinux/iso/${local.arch_version}.01/archlinux-${local.arch_version}.01-x86_64.iso"
  iso_checksum = "file:https://mirrors.kernel.org/archlinux/iso/${local.arch_version}.01/sha1sums.txt"
  ssh_username = "root"
  ssh_password = "${local.ssh_password}"
  boot_wait = "45s"
  ssh_timeout = "40m"
  shutdown_command = "sudo systemctl poweroff"
  boot_command = [
    "reflector --country Romania --save /etc/pacman.d/mirrorlist<enter>",
    "pacman -Sy --noconfirm openssh python python-pip<enter>",
    "printf \"%s\\n%s\\n\" ${local.ssh_password} ${local.ssh_password} | passwd<enter>",
    "systemctl start sshd<enter>"
//    "curl -O http://{{ .HTTPIP }}:{{ .HTTPPort }}/../alis/alis.sh<enter>",
//    "chmod +x ./alis.sh && ./alis.sh alis_create_partition /dev/sda && ./alis.sh alis_pacstrap && ./alis.sh alis_config_minimum <enter>",
  ]
}

build {

  sources = ["sources.virtualbox-iso.arch-linux"]

  provisioner "file"{
    source = "alis"
    destination = "/alis"
  }

  provisioner "shell" {
    inline = [
      "python /alis --auto /dev/sda --sec-locale ro_RO",
      "python /alis conf admin-user admin admin",
      "python /alis install --user admin --pre-config nano docker",
    ]
  }

//  provisioner "shell" {
//    inline = [
//      "pip install --index-url https://test.pypi.org/simple/ --no-deps alis-draducanu",
//      "alis --auto /dev/sda"
//    ]
//  }

  post-processor "vagrant" {
    output = "${local.output_box}/alis-{{ .Provider }}-${local.arch_version}.box"
  }
}
