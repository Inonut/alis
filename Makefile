PROJECT_NAME = alis-tool

validate-pkr.hcl:
	packer validate vagrant/alis.pkr.hcl

build-box:
	packer build -force vagrant/alis.pkr.hcl

test-vagrant:
	vagrant box add output_box/alis-virtualbox-*.box --force --name alis
	VAGRANT_CWD=vagrant vagrant destroy -f
	VAGRANT_CWD=vagrant vagrant up

build-test:
	python setup.py sdist bdist_wheel $(PROJECT_NAME)-$(PYPI_TEST_USERNAME) $(VERSION)

publish-test:
	twine upload --skip-existing --repository testpypi -u $(PYPI_TEST_USERNAME) -p $(PYPI_TEST_PASSWORD) dist/*

build-release:
	python setup.py sdist bdist_wheel $(PROJECT_NAME) $(VERSION)

publish-release:
	twine upload -u $(PYPI_TEST_USERNAME) -p $(PYPI_TEST_PASSWORD) dist/*
